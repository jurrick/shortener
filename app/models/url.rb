class Url < ApplicationRecord
  # CLASS METHODS
  def self.get(short_url)
    find(Ruby::ShortUrl::Encoder.new.decode_url(short_url))
  end

  # INSTANCE METHODS
  def short_url
    @short_url ||= Ruby::ShortUrl::Encoder.new.encode_url(id)
  end

  def increment_viewed!
    increment!(:viewed)
  end
end
