class UrlsController < ApplicationController
  before_action :find_url, only: %i[show stats]

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def create
    render plain: Url.create(url_params).short_url
  end

  def show
    @url.increment_viewed!
    render plain: @url.link
  end

  def stats
    render plain: "Количество переходов: #{@url.viewed}"
  end

  private

  def url_params
    params.require(:url).permit(:link)
  end

  def find_url
    @url = Url.get(params[:short_url])
  end

  def record_not_found
    render plain: 'Короткий URL не найден.'
  end
end
