Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  controller :urls do
    post '/urls' => :create

    constraints(short_url: /[[:alnum:]]+/) do
      get '/urls/:short_url' => :show
      get '/urls/:short_url/stats' => :stats
    end
  end
end
