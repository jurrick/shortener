# Сервис коротких ссылок

Используемые технологии:

* Ruby - 3.0.1
* Rails - 6.1.3
* ruby-short_url - гем для умного укорачивания ссылок.

## Пример запросов

Создание короткого URL

```bash
$ curl -X POST 'http://localhost:3000/urls?url\[link\]=http://yandex.ru'
# => 08nZ2
```

Получение длинного URL по короткому

```bash
$ curl 'http://localhost:3000/urls/08nZ2'
# => http://yandex.ru
```

Получение количества переходов по короткому URL:

```bash
curl 'http://localhost:3000/urls/08nZ2/stats'
# => Количество переходов: 1
```
